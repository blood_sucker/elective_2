/**
*
* Al Lestaire Acasio
* Date Modified : 7-26-2015 1:48 PM
*/

myApp.service('buttonsServices', function($filter) {
	
	this.getButton = function() {
    	return buttons
    }

    this.getTotalValue = function() {
    	return TOTALVALUE
    }

    this.getTemporary = function() {
    	return TEMP
    }

    this.updateValue = function(a) {

        var x = {}
        var y = ''
        var z;
        switch(typeof(a)) {
            case 'number':
                switch(CLEAN_INPUT) {
                    case true:
                        TOTALVALUE  = $filter('formatSign')(a, FLOAT)
                        CLEAN_INPUT = false
                        break
                    case false:
                        if( String(TOTALVALUE).length <= 15 )
                        {
                            TOTALVALUE =  $filter('formatSign')(String(TOTALVALUE) + String(a), FLOAT)
                        }
                        break
                }
                break
            case 'string':
                x = $filter('operations')(a)
                
                switch(x.isOperator) {
                    case true:
                        if(!EQUALS) {
                        	if( TEMP != 0 && OPERATOR != '' )
	                        {
								z           = $filter('equation')(TEMP, OPERATOR, $filter('formatSign')(TOTALVALUE, FLOAT, true))
								TOTALVALUE  = $filter('formatSign')(z, FLOAT)
							}
								TEMP        = $filter('formatSign')(TOTALVALUE, FLOAT, true)
								OPERATOR    = x.operation
								CLEAN_INPUT = true
						} else if (EQUALS) {
                            OPERATOR    = x.operation
                            EQUALS      = false
                            CLEAN_INPUT = true
						}
                        break
                    case false:
                        y = String(TOTALVALUE)

                        switch(x.operation) {
                            case 'backspace':
                                if(TOTALVALUE != 0 && y.length > 1) {
                                    TOTALVALUE = $filter('formatSign')(y.substring(0, y.length-1), FLOAT)
                                } else { TOTALVALUE = 0; CLEAN_INPUT = true }
                                break
                            case 'clean':
                                TOTALVALUE  = 0
                                CLEAN_INPUT = true
                                break
                            case 'clean-all':
                                TOTALVALUE  = 0
                                DECIMAL     = false
                                FLOAT       = false
                                TEMP        = 0
                                OPERATOR    = ''
                                CLEAN_INPUT = true
                                break
                            case 'sign':
                                TOTALVALUE = $filter('formatSign')($filter('changeSign')($filter('formatSign')(TOTALVALUE, FLOAT, true)), FLOAT)
                                break
                            case 'square-root':
                                TOTALVALUE = Math.sqrt($filter('formatSign')(TOTALVALUE, FLOAT, true))
                                break
                            case 'decimal':
                                DECIMAL = true
                                FLOAT   = true
                                TOTALVALUE = TOTALVALUE + '.'
                                break
                            case 'equals':
                            	if(OPERATOR != '') {
                                    z          = $filter('equation')(TEMP, OPERATOR, $filter('formatSign')(TOTALVALUE, FLOAT, true))
                                    TOTALVALUE = $filter('formatSign')(z, FLOAT)
                                    DECIMAL    = false
                                    FLOAT      = false
                                    TEMP       = $filter('formatSign')(TOTALVALUE, FLOAT, true)
                                    EQUALS     = true
                            	}
                            	break
                        }

                }
                break
        }
    }

    var FLOAT       = false
    var DECIMAL     = false
    var EQUALS      = false
    var CLEAN_INPUT = true
    var OPERATOR    = ''
    var TEMP        = 0
    var TOTALVALUE  = 0

	var buttons = [
        {row: [
            {id: 'backspace', text: '<--'},
            {id: 'clean', text: 'CE'},
            {id: 'clean-all', text: 'C'},
            {id: 'sign', text: '+/-'},
            {id: 'square-root', text: 'sqrt'}
        ]},
        {row: [
            {id: 7, text: 7},
            {id: 8, text: 8},
            {id: 9, text: 9},
            {id: 'division', text: '/'},
            {id: 'modulo', text: '%'}
        ]},
        {row: [
            {id: 4, text: 4},
            {id: 5, text: 5},
            {id: 6, text: 6},
            {id: 'multiply', text: '*'},
            {id: 'reciprocal', text: '1/x'}
        ]},
        {row: [
            {id: 1, text: 1},
            {id: 2, text: 2},
            {id: 3, text: 3},
            {id: 'subtract', text: '-'},
            {id: 'equals', text: '=', row: 2, style: 'height: 122px'}
        ]},
        {row: [
            {id: 0, text: 0, col: 2, style: 'width: 94%'},
            {id: 'decimal', text: '.'},
            {id: 'add', text: '+'}
        ]}
    ]



})