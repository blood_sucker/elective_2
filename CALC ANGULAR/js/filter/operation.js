/**
*
* Al Lestaire Acasio
* Date Modified : 7-26-2015 1:48 PM
*/


myApp.filter('operations', function() {

    return function(text) {
        var toReturn = {}
        var LIST = {
            division : '/',
            multiply : '*',
            subtract : '-',
            add : '+'
        }
        switch(text) {
            case 'division':
            case 'multiply':
            case 'subtract':
            case 'add':
                toReturn.operation = LIST[text]
                toReturn.isOperator = true
                break
            default :
                toReturn.operation = text
                toReturn.isOperator = false
                break
        }

        return toReturn
    }
})