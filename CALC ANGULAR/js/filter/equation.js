
/**
*
* Al Lestaire Acasio
* Date Modified : 7-26-2015 1:48 PM
*/


myApp.filter('equation', function() {

    return function(first, operator, second) {
        var anser = 0
        switch(operator) {
            case '+':
                anser = first + second
                break
            case '-':
                anser = first - second
                break
            case '*':
                anser = first * second
                break
            case '/':
                anser = first / second
                break
        }

        return anser
    }
})