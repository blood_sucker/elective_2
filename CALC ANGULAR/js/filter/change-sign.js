/**
*
* Al Lestaire Acasio
* Date Modified : 7-26-2015 1:48 PM
*/

myApp.filter('changeSign', function() {
	return function(number) {
		if( number != 0 ) {
			return number * -1
		} else {
			return number
		}
	}
})