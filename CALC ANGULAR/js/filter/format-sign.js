/**
*
* Al Lestaire Acasio
* Date Modified : 7-26-2015 1:48 PM
*/


/**
*
* Place the sign in front as a first letter by putting the negative sign into the last letter
* This happen because of CSS, which I use the property align-text to RIGHT
*/
myApp.filter('formatSign', function() {

    return function(text, isFloat, isRealNumber = false) {
        var x = 0

        if(String(text).contains('-')) {
            var y = String(text).split('-').join('')
            x = (isFloat) ? parseFloat(y) * -1 : parseInt(y) * -1
        } else {
            x = (isFloat) ? parseFloat(text) : parseInt(text)
        }

        if(x < 0) {
            return (isRealNumber) ? x : String(x).substring(1,String(x).length) + '-'
        } else {
            return x
        }
    }
})