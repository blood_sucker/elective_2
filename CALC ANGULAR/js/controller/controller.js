/**
*
* Al Lestaire Acasio
* Date Modified : 7-26-2015 1:48 PM
*/

/**
*
* A controller that controlls the inputs which has a services to manage data.
*/
myApp.controller('calculatorController', function($scope, $filter, buttonsServices) {
    $scope.tableRow = buttonsServices.getButton()

    $scope.totalValue  = $filter('formatSign')(0)

    $scope.numberBtn = function(a) {
        buttonsServices.updateValue(a)

        $scope.totalValue = buttonsServices.getTotalValue()
    }
})