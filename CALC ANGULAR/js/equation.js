/**
*
* Author: Al Lestaire G. Acasio
* Date updated : 7-19-2015 11:10 AM
*
*/

$(document).ready(function () {

	var LIST_KEY    = new Array()
	var start       = 96
	var TEMP        = 0
	var OPERATOR    = ""
	var CLEAR_INPUT = true
	var DECIMAL     = false
	var FLOAT       = false
	var EQUALS		= false

	LIST_KEY[61]  = "addition"
	LIST_KEY[56]  = "multiply"
	LIST_KEY[191] = "division"
	LIST_KEY[173] = "subtract"
	LIST_KEY[111] = "division"
	LIST_KEY[106] = "multiply"
	LIST_KEY[107] = "addition"
	LIST_KEY[109] = "subtract"
	LIST_KEY[13]  = "equal";

	for(var i=0; i<=9; i++)
	{
		LIST_KEY[start] = i
		start++
	}

	start = 96;



	$(".numbers").click(function() {
		var numbers = $("#num").val()
		var text = $(this).text()
		
		
		

		if($.trim(text) == "." && !DECIMAL) {
			DECIMAL = true
			FLOAT = true
			update_equation(numbers, text)
		} else if ( $.trim(text) != ".") {
			update_equation(numbers, text)
		}
		
		CLEAR_INPUT = false
		
	})

	$(".equation").click(function() {
		var q = $(this).text()

		if( !CLEAR_INPUT )
		{
			if( TEMP != 0 && OPERATOR != "")
			{
				$("#num").val(calculate(TEMP, OPERATOR))
			}

			OPERATOR    = $.trim(q)
			TEMP        = format_sign()
			CLEAR_INPUT = true
			DECIMAL     = false
		} else if ( EQUALS )
		{
			OPERATOR = $.trim(q)
			EQUALS   = false
		}
		
	})

	$("#clean").click(function() { $("#num").val("0"); CLEAR_INPUT = true})
	$("#clean-all").click(function(){
		$("#num").val("0")
		DECIMAL     = false
		FLOAT       = false
		TEMP        = 0
		OPERATOR    = ""
		CLEAR_INPUT = true
	})

	
	$("body").keydown(function(event) {
		var code = (event.keyCode) ? event.keyCode : event.which
		var str = $("#num").val()
		var bckspace = 0

		if( parseInt(str) != 0 )
		{
			bckspace = (event.keyCode) ? event.keyCode : event.which
			if( bckspace == 8 )
			{
				if(str.length == 1 && parseInt(str) > 0)
					str = 0
				else str = str.substring(0, str.length-1)

				$("#num").val(str)
			}
		}
		
		$("#" + LIST_KEY[code]).trigger("click");
	})

	$("#equals").click(function() {

		if( OPERATOR != "" )
		{
			$("#num").val(calculate(TEMP, OPERATOR))

			DECIMAL     = false
			CLEAR_INPUT = true
			TEMP        = format_sign()
			EQUALS 		= true
		}

	})

	$("#square-root").click(function() {

		$("#num").val(Math.sqrt(parseInt($("#num").val())))
	})

	$("#sign").click(function() {
		var text = $("#num").val()
		if( $.trim(text).indexOf('-') > 0 )
		{
			$("#num").val($.trim(text).substring(0,$.trim(text).indexOf('-')))
		} else {
			$("#num").val($.trim(text) + "-")
		}
	})

	function update_equation (a, b)
	{
		
		var x = "";

		if( a.length <= 15 )
		{
			if( CLEAR_INPUT )
			{
				x = b
			} else {
				
				x = a + b
			}
			$("#num").val(x)
		}
	}

	function format_sign()
	{
		var text = $("#num").val()
		var number = 0

		if( $.trim(text).indexOf('-') > 0 )
		{

			number = (FLOAT) ? parseFloat($.trim(text).substring(0,$.trim(text).indexOf('-'))) : parseInt($.trim(text).substring(0,$.trim(text).indexOf('-')))
			return  number * -1
		} else {
			number = (FLOAT) ? parseFloat($.trim(text)) : parseInt($.trim(text))
			return number
		}
	}

	function calculate (tmp, operator)
	{
		var anser
		var num = format_sign()

		switch(operator)
		{
			case '+':
				anser = tmp + num
				break
			case '-':
				anser = tmp - num
				break
			case '*':
				anser = tmp * num
				break
			case '/':
				anser = tmp / num
				break
		}

		if( anser < 0 )
		{
			anser = anser * -1
			anser = anser + "-"
		}
		return anser;
	}
});