app.filter('filterByCategory', [function() {
	return function(rent, activeCategory) {

		var holder = []
		if( activeCategory == 0 ) {
			return rent
		} else {
			angular.forEach(rent, function(item) {
				if(item.category == activeCategory) {
					holder.push(item)
				}
			})

			return holder
		}
	}
}])


app.filter('filterOwnerIDbyName', ['AccountServices',function(AccountServices) {

	return function(ownerId) {
		var account = AccountServices.getInfoById(ownerId)

		return account.fname
	}
}])


app.filter('uppercaseFirstChar', [function() {
	return function(text) {
		var holder = [],
			texts = text.split(' ')

			angular.forEach(texts, function(letters) {
				holder.push(letters.substring(0,1).toUpperCase() + letters.substring(1, letters.length))
			})

			return holder.join(' ')
	}
}])