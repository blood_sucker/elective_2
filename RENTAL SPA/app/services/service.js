(function() {
	app.service('spaceDataServices', [function() {
		this.getSpaces = function() {
			return spaces
		}

		this.getSpaceById = function(id) {
			return spaces[id]
		}

	    this.getSpaceByOwnerId = function(id) {
	        var holder = []

	        angular.forEach(spaces, function(space) {
	            if(space.ownerID == id) {
	                holder.push(space)
	            }
	        })

	        return holder
	    }

		var spaces = []

		spaces = [
			{
	            ownerID: 1,
				title: 'Lawesbra Rental',
				image: 'img/img1.jpg',
				category: 1,
				description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi doloremque odio quidem expedita veritatis dolore ratione consequuntur ducimus harum? Non esse dicta culpa commodi maiores quaerat iste ipsa id repudiandae?'
			},
			{
	            ownerID: 1,
				title: 'Cogon Rental',
				image: 'img/img1.jpg',
				category: 1,
				description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi doloremque odio quidem expedita veritatis dolore ratione consequuntur ducimus harum? Non esse dicta culpa commodi maiores quaerat iste ipsa id repudiandae?'
			},
			{
	            ownerID: 1,
				title: 'Carmen Rental',
				image: 'img/img1.jpg',
				category: 1,
				description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi doloremque odio quidem expedita veritatis dolore ratione consequuntur ducimus harum? Non esse dicta culpa commodi maiores quaerat iste ipsa id repudiandae?'
			},
			{
	            ownerID: 1,
				title: 'Lapasan Rental',
				image: 'img/img1.jpg',
				category: 1,
				description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi doloremque odio quidem expedita veritatis dolore ratione consequuntur ducimus harum? Non esse dicta culpa commodi maiores quaerat iste ipsa id repudiandae?'
			},
			{
	            ownerID: 1,
				title: 'Kauswagan Rental',
				image: 'img/img1.jpg',
				category: 2,
				description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi doloremque odio quidem expedita veritatis dolore ratione consequuntur ducimus harum? Non esse dicta culpa commodi maiores quaerat iste ipsa id repudiandae?'
			},
			{
	            ownerID: 2,
				title: 'Bugo Rental',
				image: 'img/img1.jpg',
				category: 2,
				description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi doloremque odio quidem expedita veritatis dolore ratione consequuntur ducimus harum? Non esse dicta culpa commodi maiores quaerat iste ipsa id repudiandae?'
			},
			{
	            ownerID: 2,
				title: 'Zayas Rental',
				image: 'img/img1.jpg',
				category: 2,
				description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi doloremque odio quidem expedita veritatis dolore ratione consequuntur ducimus harum? Non esse dicta culpa commodi maiores quaerat iste ipsa id repudiandae?'
			},
			{
	            ownerID: 2,
				title: 'Upper Carmen Rental',
				image: 'img/img1.jpg',
				category: 3,
				description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi doloremque odio quidem expedita veritatis dolore ratione consequuntur ducimus harum? Non esse dicta culpa commodi maiores quaerat iste ipsa id repudiandae?'
			},
			{
	            ownerID: 2,
				title: 'Gusa Rental',
				image: 'img/img1.jpg',
				category: 3,
				description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi doloremque odio quidem expedita veritatis dolore ratione consequuntur ducimus harum? Non esse dicta culpa commodi maiores quaerat iste ipsa id repudiandae?'
			},
			{
	            ownerID: 2,
				title: 'Mologan Rental',
				image: 'img/img1.jpg',
				category: 3,
				description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi doloremque odio quidem expedita veritatis dolore ratione consequuntur ducimus harum? Non esse dicta culpa commodi maiores quaerat iste ipsa id repudiandae?'
			}
		]
	}])


	app.service('NavigationService', [function() {

		this.getMainNav = function() {
			return mainNav
		}

		this.getCategory = function() {
			return categoryNav
		}

		this.setAccount = function(accountData) {
			account = {username: accountData.username, password: accountData.password, status: accountData.status}
		}

		this.getLoginNav = function() {
			return loginNav
		}

		var mainNav = [],
			categoryNav = [],
			loginNav = [];

		mainNav = [
			{text: 'HOME', link: '/home', icon: 'glyphicon-home'},
			{text: 'FIND SPACES', link: '/spaces', icon: 'glyphicon-eye-open'},
			{text: 'LOGIN', link: '/login', icon: 'glyphicon-tag'},
			{text: 'REGISTER', link: '/register', icon: 'glyphicon-cog'},
		]



		loginNav = [
			{text: 'HOME', link: '/home', icon: 'glyphicon-home'},
			{text: 'MANAGE BUSINESS', link: '/business', icon: 'glyphicon-stats'}
		]
		categoryNav = ['View all', 'Apartle', 'Boarding House', 'Hotel', 'Motel']
	}])


	app.service('AccountServices', [function() {

		this.isExistUsername = function(username) {
			var checking = true
			angular.forEach(accounts, function(a) {
				if(a.username === username) {
					checking = false
				}
			})
			return checking
		}

		this.getAccount = function(username, password) {

			var toReturn = false
			angular.forEach(accounts, function(acc) {
				if(acc.username == username && acc.password == password) {
					
					toReturn = true
					logAccountInfo = acc
				}
			})

			return toReturn
		}

	    this.getInfoById = function(id) {
	        var info = {}

	        angular.forEach(accounts, function(acc) {
	            if(acc.id == id) info = acc
	        })

	        return info
	    }

	    this.getInfo = function(username, password) {
	        var info = {}
	        angular.forEach(accounts, function(acc) {
	            if(acc.username == username && acc.password == password) {
	                
	                info = acc
	            }
	        })

	        return info
	    }

	    this.getAccounts = function() {
	        return accounts
	    }

	    this.addAccount = function(newAccount) {
	        accounts.push(newAccount)
	    }

	    this.getLogAccountInfo = function() {
	    	return logAccountInfo
	    }

	    this.clearAccountLog = function() {
	    	logAccountInfo = {}
	    }

		var accounts = [],
			logAccountInfo = {}

		accounts = [
			{
	            id: 1,
	            fname: 'Losewin',
	            business: 'Losewin Business',
	            gender: 'Male',
	            email: 'losewin@gmail.com',
				username: 'losewin',
				password: 'losewinniwesol'
			},
			{
	            id: 2,
	            fname: 'Owner',
	            business: 'Owner lamang',
	            gender: 'Female',
	            email: 'owner@gmail.com',
				username: 'owner',
				password: 'ownerrenwow',
	        }
		]
	}])
})()