app.directive('ensureUnique', ['AccountServices', function(AccountServices) {
	return {
		require: 'ngModel',
		link: function(scope, ele, attr, c) {
			scope.$watch(attr.ngModel, function(n) {
				if(!n) return
				c.$setValidity('unique', AccountServices.isExistUsername(n))
			})
		}
	}
}])