app.controller('SpaceController', ['$scope', 'spaceDataServices', 'NavigationService',
	function($scope, spaceDataServices, NavigationService) {

	$scope.rentals        = spaceDataServices.getSpaces()
	$scope.activeCategory = 0
	$scope.listOfCategory = NavigationService.getCategory()
	$scope.viewCategory   = function(a) {
		$scope.activeCategory = a
	}

	$scope.active = function(a) {
		if( a == $scope.activeCategory) {
			return true
		} else {
			return false
		}
	}
}])

app.controller('NavigationController', ['$scope', '$location', '$rootScope', 'NavigationService', 'AccountServices',
	function($scope, $location, $rootScope, NavigationService, AccountServices) {

	$scope.hasAccount = false
	$scope.mainNav = NavigationService.getMainNav()
	$scope.loginNav = NavigationService.getLoginNav()
	$scope.ownerName = ''

	$scope.logout = function() {
		AccountServices.clearAccountLog()
		$scope.hasAccount = false
		$location.path('/home')
	}
	
	$rootScope.$on('changeNav', function() {
		var info = {}

		$scope.hasAccount = true
		info = AccountServices.getLogAccountInfo()

		$scope.ownerName = info.fname
	})

	$scope.getClass = function(path) {
		if($location.path().substr(0, path.length) == path) {
			return true
		} else {
			return false
		}
	}
}])


app.controller('SpaceDetailController', ['$scope', '$routeParams', 'spaceDataServices',
	function($scope, $routeParams, spaceDataServices) {

	$scope.spaceInfo = spaceDataServices.getSpaceById($routeParams.itemIndex)
}])

app.controller('RegistrationController', ['$scope', 'AccountServices', '$location', '$filter',
	function($scope, AccountServices, $location, $filter) {

	$scope.newAccount = {}
	$scope.submitted = false
	$scope.signupForm = function() {
		if($scope.signup_form.$valid) {
			console.log('Submit')
			AccountServices.addAccount($scope.newAccount)
			$scope.signup_form.submitted = false

			delete $scope.newAccount.confirmpass

			$scope.newAccount.fname = $filter('uppercaseFirstChar')($scope.newAccount.fname)
			$scope.newAccount.business = $filter('uppercaseFirstChar')($scope.newAccount.business)
			AccountServices.getInfo($scope.newAccount.username, $scope.newAccount.password)
			$location.path('/home')
		} else {
			console.log('Dont submit')
			$scope.signup_form.submitted = true
		}
	}

}])


app.controller('LoginController', ['$scope', '$location', '$rootScope', 'AccountServices',
	function($scope, $location, $rootScope, AccountServices) {

	$scope.msg = ''
	$scope.login = function() {

		var isExist = AccountServices.getAccount($scope.login.username, $scope.login.password)
		if(isExist) {
			$scope.msg = 'You are exist'
			$rootScope.$broadcast('changeNav')
			$location.path('/business');
		} else {
			$scope.msg = 'You are not existing'
		}
		$scope.login.password = ''
		$scope.login.username = ''
	}
}])


app.controller('OwnerController', ['$scope', 'spaceDataServices', 'AccountServices',
	function($scope, spaceDataServices, AccountServices) {

		var accountInfo     = AccountServices.getLogAccountInfo()
		$scope.ownerRentals = spaceDataServices.getSpaceByOwnerId(accountInfo.id)
}])