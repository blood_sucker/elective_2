var app = angular.module('RENSTING-SYSTEM', ['ngRoute', 'ngAnimate'])

app.config(['$routeProvider', function($routeProvider) {

	$routeProvider
		.when('/home',
            {
                controller: 'DefaultController',
                templateUrl: 'partial/home.html'
            })
        .when('/spaces',
            {
                controller: 'SpaceController',
                templateUrl: 'partial/spaces.html'
            })
        .when('/login',
            {
                controller: 'LoginController',
                templateUrl: 'partial/login.html'
            })
        .when('/space-detail/:itemIndex',
            {
                controller: 'SpaceDetailController',
                templateUrl: 'partial/detail.html'
            })
        .when('/register',
            {
                controller: 'RegistrationController',
                templateUrl: 'partial/register.html'
            })
        .when('/business',
            {
                controller: 'OwnerController',
                templateUrl: 'partial/manage-business.html'
            })
        .otherwise( {redirectTo: '/home'} )
}])